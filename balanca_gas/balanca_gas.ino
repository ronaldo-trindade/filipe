#include "HX711.h"
#include <Wire.h>

 #define DT_PIN1 18
 #define SCK_PIN1 17

 #define DT_PIN2 23
 #define SCK_PIN2 16


HX711 balanca1; /* instancia Balança HX711 */
HX711 balanca2; /* instancia Balança HX711 */

float calibration_factor1 = -21.8715; /* fator de calibração aferido na Calibração */
float calibration_factor2 = -21.5218; /* fator de calibração aferido na Calibração */

void instrucoes(){
  Serial.println("Pressione z para zerar as balancas");                   
  Serial.println("Pressione 1 para calibrar balanca 1");                   
  Serial.println("Pressione 2 para calibrar balanca 2");                                                

}

void setup() {
  Serial.begin(9600);
  balanca1.begin(DT_PIN1, SCK_PIN1); /* inicializa a balança 1  */
  balanca2.begin(DT_PIN2, SCK_PIN2); /* inicializa a balança 2  */
  balanca1.set_scale(calibration_factor1); /* ajusta fator de calibração  */
  balanca2.set_scale(calibration_factor2); /* ajusta fator de calibração  */
  // não zerar as balancas pois só será trocado um botujão por vez!
  //balanca1.tare();                        /* zera a Balança  */
  //balanca2.tare(); 
  //offset = balanca.read_average(3); //tira uma média de 3 amostras para o offset
  delay(1000);
  Serial.println(" Balancas  inicializadas!");
  delay(1000);
  instrucoes();
}

void loop() {
  float peso = 0;
  Serial.print("Peso 1 ");
  peso = pesar(&balanca1);  
  Serial.print(peso, 2); /* imprime peso na balança com 2 casas decimais */
  Serial.println(" kg"); /* imprime no monitor serial */
  delay(1000);
  Serial.print("Peso 2 ");
  peso = pesar(&balanca2);  
  Serial.print(peso, 2); /* imprime peso na balança com 2 casas decimais */
  Serial.println(" kg"); /* imprime no monitor serial */

  
  delay(1000);


  if (Serial.available()) /* se a serial estiver disponivel */
  {
    char temp = Serial.read();      /* le caracter da serial  */
    if (temp == 'z' || temp == 'Z') /* se pressionar t ou T   */
    {
      balanca1.tare();                    /* zera a balança   */
      balanca2.tare();
      Serial.println(" Balança zeradas"); /* imprime no monitor serial  */
    }

    if (temp == '1') {  // calibrar balança 1
      Serial.println(" Calibrando Balança 1"); 
      calibrar(&balanca1);
    }
    if (temp == '2') {  // calibrar balança 2
      Serial.println(" Calibrando Balança 2"); 
      calibrar(&balanca2);
    }
    if (temp == '?') {  // calibrar balança 2
      instrucoes();
    }
  }
}

float pesar(HX711 *balanca){
  float peso = 0;
  
  peso = balanca->get_units(20);  // - offset;
  peso = peso / 1000;

  return peso;
}


void calibrar(HX711 *balanca) {

  if (balanca->is_ready()) {
    balanca->set_scale();    
    Serial.println("Tarar... remova qualquer peso da balanca->");
    delay(5000);
    balanca->tare();
    Serial.println("Tara feita...");
    Serial.print("coloque o peso conhecido na balança...");
    delay(5000);
    long reading = balanca->get_units(10);
    Serial.print("Resultad0: ");
    Serial.println(reading);
    Serial.println("O fator de calibração deverá ser: (Resultado)/(peso conhecido");   
    Serial.println("--------------------------------------------------------------");   
    Serial.println("Introduza o fator de calibração no código e recompile o mesmo!");   
    Serial.println(""); 
    while (1){};     

  } 
  else {
    Serial.println("HX711 não encontrado!");
  }
  delay(1000);
}
